package com.agiletestingalliance;

import static org.junit.Assert.*;
import org.junit.Test;


public class MinMaxTest {

    @Test
    public void testReturnMax() throws Exception {
        int maxVal = new MinMax().returnMax(1, 2);
        assertEquals("Add", 2, maxVal);
    }

} 
