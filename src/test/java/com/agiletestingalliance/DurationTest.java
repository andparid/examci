package com.agiletestingalliance;

import static org.junit.Assert.*;
import org.junit.Test;


public class DurationTest {

    @Test
    public void testDur() throws Exception {
        String dur = new Duration().dur();
        assertEquals("Add", true, dur.contains("then you can opt for either half days"));
    }

} 
