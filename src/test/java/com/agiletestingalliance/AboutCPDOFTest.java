package com.agiletestingalliance;

import static org.junit.Assert.*;
import org.junit.Test;


public class AboutCPDOFTest {

    @Test
    public void testDesc() throws Exception {
        String desc = new AboutCPDOF().desc();
        assertEquals("Add", true, desc.contains("DevOps Life Cycle practically"));
    }

} 
