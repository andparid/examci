package com.agiletestingalliance;

public class Usefulness{

    public String desc() {
        return "DevOps is about transformation, about building quality in, improving productivity and about automation in Dev, Testing and Operations. <br> <br> CP-DOF is a one of its kind initiative to marry 2 distinct worlds of Agile and Operations together. <br> <br> <b>CP-DOF </b> helps you learn DevOps fundamentals along with Continuous Integration and Continuous Delivery and deep dive into DevOps concepts and mindset.";

  	}

    public void functionWF() {
        int checker = 0;

	while (checker < 5){
	    checker++;
	}
        
	int counterA;
        int counterB;
	
	for (counterA = 0, counterB = 0; counterA < 5; counterA++, counterB += 2){
	    System.out.println(counterA);
	}

    }

}
